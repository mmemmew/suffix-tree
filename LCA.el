;;; lca.el --- Lowest Common Ancestor retrieval preprocessing -*- lexical-binding: t; -*-

;;; Author: Durand
;;; Created: 2021-01-22 02:31:20
;;; Version: 0.0.1
;;; Keywords: matching

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This files implements the preprocessing on a tree so that
;; subsequent lowest common ancestor retrievals can be done in
;; constant time.

;; The algorithm used is described in the book « Algorithms on
;; Strings, Trees, and Sequences: Computer Science and Computational
;; Biology » by Dan Gusfield.

;;; Code:

;;; Bit level helpers

;;;###autoload
(defun lca-gen-left-most-bit-table (size)
  "Generate a table of the most significant bits of size SIZE.

Without this table it would take time proportional to the square
of the logarithm of SIZE to find the position of the most
significant bit of all SIZE-bit binary numbers, in the worst
case.

Since SIZE is supposed to be fixed for our tree, we implement the
table as a vector."
  ;; It would probably be easier to understand by writing 2 in this
  ;; way.
  (let ((next (ash 1 1))
        (maximum (ash 1 size))
        (table (vector 0))
        (current 1)
        (current-bit 1))
    (while (<= next maximum)
      (setq table (vconcat table (vector current-bit)))
      (setq current (1+ current))
      (cond
       ((= current next)
        (setq next (ash next 1))
        (setq current-bit (1+ current-bit)))))
    table))

;;;###autoload
(defun lca-right-most-bit (num)
  "Return the position of least significant bit of NUM.

This is equivalent with (1+ (floor (log (logand num (- num)) 2))).
The `1+' is to ensure we start counting from 1."
  (1+ (floor (log (logand num (- num)) 2))))

;;;###autoload
(defun lca-preprocess (tree)
  "Return a preprocessed tree from TREE.

After this preprocess is done, subsequent lowest common ancestor
retrievals can be done in constant time.")


(provide 'lca)

;;; LCA.el ends here
